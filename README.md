# Durt2 Library

## Overview

Durt2 is a Dart library designed for [Duniter v2s](https://git.duniter.org/nodes/rust/duniter-v2s) blockchain and cryptocurrency management. It integrates various services, including Squid GraphQL, Duniter networks use, and secure wallet functionalities, making it an all-encompassing solution for duniter v2s related operations in Dart and Flutter applications.

## Features

- [Polkadart](https://pub.dev/packages/polkadart) integration for accessing Duniter blockchain functionalities.
- GraphQL integration for querying blockchain data.
- Wallet services including encryption and decryption of mnemonics.
- Secure storage and retrieval of wallet information.
- Support for multiple blockchain networks (`gdev`, `gtest`, `g1`).

## Getting Started

To use Durt2 in your project, you need to include it in your Dart or Flutter project's dependency list.

### Installation

1. Add Durt2 to your `pubspec.yaml` file:

```yaml
dependencies:
    durt2: <version>
```

2. Run the following command to get the package:

```bash
flutter pub get
```

### Initialization

To initialize the library, you need to specify the blockchain network and the endpoints for both Duniter and Squid services.

```dart
import 'package:durt2/durt2.dart';

void main() async {
  await Durt.init(
    network: Networks.gdev,
  );

  // ...
}
```
