// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'encrypted_mnemonic.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class EncryptedMnemonicAdapter extends TypeAdapter<EncryptedMnemonic> {
  @override
  final int typeId = 0;

  @override
  EncryptedMnemonic read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return EncryptedMnemonic(
      cipherText: fields[0] as String,
      iv: fields[1] as Uint8List,
    );
  }

  @override
  void write(BinaryWriter writer, EncryptedMnemonic obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.cipherText)
      ..writeByte(1)
      ..write(obj.iv);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EncryptedMnemonicAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
