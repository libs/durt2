// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wallet_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WalletDataAdapter extends TypeAdapter<WalletData> {
  @override
  final int typeId = 1;

  @override
  WalletData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WalletData(
      address: fields[2] as String,
      safeBoxNumber: fields[1] as int,
      name: fields[3] as String?,
      derivation: fields[4] as int?,
      imagePath: fields[5] as String?,
      identityStatus: fields[6] as IdtyStatus,
      balance: fields[7] as double,
      certs: (fields[8] as List).cast<int>(),
    )..key = fields[0] as WalletDataKey;
  }

  @override
  void write(BinaryWriter writer, WalletData obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.key)
      ..writeByte(1)
      ..write(obj.safeBoxNumber)
      ..writeByte(2)
      ..write(obj.address)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.derivation)
      ..writeByte(5)
      ..write(obj.imagePath)
      ..writeByte(6)
      ..write(obj.identityStatus)
      ..writeByte(7)
      ..write(obj.balance)
      ..writeByte(8)
      ..write(obj.certs);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WalletDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class WalletDataKeyAdapter extends TypeAdapter<WalletDataKey> {
  @override
  final int typeId = 2;

  @override
  WalletDataKey read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WalletDataKey(
      safeBoxNumber: fields[0] as int,
      address: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, WalletDataKey obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.safeBoxNumber)
      ..writeByte(1)
      ..write(obj.address);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WalletDataKeyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class IdtyStatusAdapter extends TypeAdapter<IdtyStatus> {
  @override
  final int typeId = 3;

  @override
  IdtyStatus read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return IdtyStatus.none;
      case 1:
        return IdtyStatus.created;
      case 2:
        return IdtyStatus.confirmed;
      case 3:
        return IdtyStatus.validated;
      case 4:
        return IdtyStatus.expired;
      case 5:
        return IdtyStatus.unknown;
      default:
        return IdtyStatus.none;
    }
  }

  @override
  void write(BinaryWriter writer, IdtyStatus obj) {
    switch (obj) {
      case IdtyStatus.none:
        writer.writeByte(0);
        break;
      case IdtyStatus.created:
        writer.writeByte(1);
        break;
      case IdtyStatus.confirmed:
        writer.writeByte(2);
        break;
      case IdtyStatus.validated:
        writer.writeByte(3);
        break;
      case IdtyStatus.expired:
        writer.writeByte(4);
        break;
      case IdtyStatus.unknown:
        writer.writeByte(5);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is IdtyStatusAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
