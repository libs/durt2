// ignore_for_file: use_build_context_synchronously

import 'package:hive/hive.dart';

part 'wallet_data.g.dart';

@HiveType(typeId: 1)
class WalletData extends HiveObject {
  @HiveField(0)
  WalletDataKey key;

  @HiveField(1)
  int safeBoxNumber;

  @HiveField(2)
  String address;

  @HiveField(3)
  String? name;

  @HiveField(4)
  int? derivation;

  @HiveField(5)
  String? imagePath;

  @HiveField(6)
  IdtyStatus identityStatus;

  @HiveField(7)
  double balance;

  @HiveField(8)
  List<int> certs;

  WalletData({
    required this.address,
    this.safeBoxNumber = 0,
    this.name,
    this.derivation,
    this.imagePath,
    this.identityStatus = IdtyStatus.unknown,
    this.balance = 0,
    this.certs = const [],
  }) : key = WalletDataKey(safeBoxNumber: safeBoxNumber, address: address);

  // representation of WalletData when debugging
  @override
  String toString() {
    return name!;
  }

  // creates the ':' separated string from the WalletData
  String inLine() {
    return "$name:$derivation:$imagePath:$identityStatus";
  }

  bool hasIdentity() {
    return identityStatus == IdtyStatus.created ||
        identityStatus == IdtyStatus.confirmed ||
        identityStatus == IdtyStatus.validated;
  }

  bool isMembre() {
    return identityStatus == IdtyStatus.validated;
  }

  bool exist() {
    return balance != 0;
  }

  bool hasImage() {
    return imagePath != null;
  }
}

@HiveType(typeId: 2)
class WalletDataKey extends HiveObject {
  @HiveField(0)
  int safeBoxNumber;

  @HiveField(1)
  String address;

  WalletDataKey({
    required this.safeBoxNumber,
    required this.address,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WalletDataKey &&
          runtimeType == other.runtimeType &&
          safeBoxNumber == other.safeBoxNumber &&
          address == other.address;

  @override
  int get hashCode => safeBoxNumber.hashCode ^ address.hashCode;
}

@HiveType(typeId: 3)
enum IdtyStatus {
  @HiveField(0)
  none,

  @HiveField(1)
  created,

  @HiveField(2)
  confirmed,

  @HiveField(3)
  validated,

  @HiveField(4)
  expired,

  @HiveField(5)
  unknown
}
