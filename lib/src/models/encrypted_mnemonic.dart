import 'dart:typed_data';
import 'package:hive/hive.dart';

part 'encrypted_mnemonic.g.dart';

@HiveType(typeId: 0)
class EncryptedMnemonic {
  @HiveField(0)
  final String cipherText;

  @HiveField(1)
  final Uint8List iv;

  EncryptedMnemonic({
    required this.cipherText,
    required this.iv,
  });
}
