import 'package:hive/hive.dart';

part 'safe_box.g.dart';

@HiveType(typeId: 4)
class SafeBox extends HiveObject {
  @HiveField(0)
  int number;

  @HiveField(1)
  String name;

  @HiveField(2)
  String defaultWalletAddress;

  @HiveField(3)
  String? imagePath;

  SafeBox({
    required this.number,
    required this.name,
    required this.defaultWalletAddress,
    this.imagePath,
  });

  @override
  String toString() => name;
}
