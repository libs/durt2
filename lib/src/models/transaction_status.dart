class TransactionStatusModel {
  final String? hash;
  final TransactionState state;
  final String? errorMessage;
  final bool isExtrinsicFailed;

  TransactionStatusModel({
    this.hash,
    this.state = TransactionState.none,
    this.errorMessage,
    this.isExtrinsicFailed = false,
  });

  const TransactionStatusModel.empty()
      : hash = null,
        state = TransactionState.none,
        errorMessage = null,
        isExtrinsicFailed = false;
}

enum TransactionState {
  none,
  pending,
  inBlock,
  finalized,
  error,
}
