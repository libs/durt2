import 'package:durt2/src/models/safe_box.dart';
import 'package:durt2/src/models/wallet_data.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';

final log = Logger();
late Box<WalletData> walletDataBox;
late Box<SafeBox> safeBox;
late Box configBox;
