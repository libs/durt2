import 'dart:async' show Stream, StreamController;
import 'dart:typed_data' show Uint8List;
import 'package:convert/convert.dart' show hex;
import 'package:durt2/src/global.dart';
import 'package:durt2/src/main.dart' show Durt;
import 'package:durt2/src/models/polkadart_generated/gdev/types/sp_runtime/multiaddress/multi_address.dart';
import 'package:durt2/src/models/transaction_status.dart';
import 'package:polkadart/apis/apis.dart';
import 'package:polkadart/extrinsic/extrinsic_payload.dart';
import 'package:polkadart/extrinsic/signature_type.dart';
import 'package:polkadart/extrinsic/signing_payload.dart';
import 'package:polkadart/primitives/primitives.dart';
import 'package:polkadart_keyring/polkadart_keyring.dart';
import 'package:ss58/ss58.dart' show Address;

class TransactionService {
  Stream<TransactionStatusModel> pay({
    required KeyPair keypair,
    required String dest,
    required double amount,
  }) async* {
    final payload =
        await _createTransactionPayload(keypair.address, dest, amount);
    final signedExtrinsic = _signExtrinsic(keypair, payload);

    yield* _submitAndWatchTransaction(signedExtrinsic);
  }

  Future<SigningPayload> _createTransactionPayload(
      String from, String dest, double amount) async {
    final runtimeVersion =
        await Durt.instance.gdev.rpc.state.getRuntimeVersion();
    final blockHash = await _getBlockHash();
    final genesisHash = await _getGenesisHash();
    final blockNumber = await _getBlockNumber();
    final nonce = await _getAccountNonce(from);

    final multiDest = const $MultiAddress().id(Address.decode(dest).pubkey);
    final runtimeCall = Durt.instance.gdev.tx.balances
        .transferKeepAlive(dest: multiDest, value: BigInt.from(amount * 100));
    final encodedCall = runtimeCall.encode();

    return SigningPayload(
      blockHash: blockHash,
      blockNumber: blockNumber,
      nonce: nonce,
      specVersion: runtimeVersion.specVersion,
      transactionVersion: runtimeVersion.transactionVersion,
      genesisHash: genesisHash,
      method: encodedCall,
      eraPeriod: 64,
      tip: 0,
    );
  }

  Future<String> _getBlockHash() async {
    return (await Durt.instance.polkadartProvider
            .send('chain_getBlockHash', []))
        .result
        .replaceAll('0x', '');
  }

  Future<String> _getGenesisHash() async {
    return (await Durt.instance.polkadartProvider
            .send('chain_getBlockHash', [0]))
        .result
        .replaceAll('0x', '');
  }

  Future<int> _getBlockNumber() async {
    final block =
        await Durt.instance.polkadartProvider.send('chain_getBlock', []);
    return int.parse(block.result['block']['header']['number']);
  }

  Future<int> _getAccountNonce(String address) async {
    return await SystemApi(Durt.instance.polkadartProvider)
        .accountNextIndex(address);
  }

  Uint8List _signExtrinsic(KeyPair keypair, SigningPayload signingPayload) {
    final srPayload = signingPayload.encode(Durt.instance.gdev.registry);
    final signature = keypair.sign(srPayload);

    return ExtrinsicPayload(
      signer: Uint8List.fromList(keypair.bytes()),
      method: signingPayload.method,
      signature: signature,
      eraPeriod: 64,
      blockNumber: signingPayload.blockNumber,
      nonce: signingPayload.nonce,
      tip: 0,
    ).encode(Durt.instance.gdev.registry, SignatureType.sr25519);
  }

  Stream<TransactionStatusModel> _submitAndWatchTransaction(
      Uint8List signedExtrinsic) async* {
    yield TransactionStatusModel(
      hash: hex.encode(signedExtrinsic),
      state: TransactionState.pending,
    );

    final controller = StreamController<TransactionStatusModel>();

    try {
      final subscription =
          await Durt.instance.authorApi.submitAndWatchExtrinsic(
        signedExtrinsic,
        (data) {
          TransactionStatusModel status =
              _mapTransactionStatus(data, signedExtrinsic);
          controller.add(status);
          if (status.state == TransactionState.finalized ||
              status.state == TransactionState.error) {
            controller.close();
          }
        },
      );

      yield* controller.stream;
      await subscription.cancel();
    } catch (e) {
      log.e('Error during transaction: $e');
      final status = TransactionStatusModel(
        hash: hex.encode(signedExtrinsic),
        state: TransactionState.error,
        errorMessage: 'Transaction failed: $e.',
      );
      controller.addError(status);
      yield* controller.stream;
    } finally {
      await controller.close();
    }
  }

  TransactionStatusModel _mapTransactionStatus(
      ExtrinsicStatus data, Uint8List signedExtrinsic) {
    final hash = hex.encode(signedExtrinsic);

    switch (data.type) {
      case 'future':
      case 'ready':
      case 'broadcast':
        return TransactionStatusModel(
            hash: hash, state: TransactionState.pending);

      case 'inBlock':
        return TransactionStatusModel(
            hash: hash, state: TransactionState.inBlock);

      case 'finalized':
        return TransactionStatusModel(
            hash: hash, state: TransactionState.finalized);

      case 'retracted':
      case 'finalityTimeout':
      case 'usurped':
      case 'dropped':
      case 'invalid':
        return TransactionStatusModel(
          hash: hash,
          state: TransactionState.error,
          errorMessage: data.type,
        );

      default:
        return TransactionStatusModel(
          hash: hash,
          state: TransactionState.error,
          errorMessage: 'Unknown status: ${data.type}',
        );
    }
  }
}
