import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:durt2/src/global.dart';

class DuniterService {
  final List<String> errorLogs = [];
  int testedEndpoints = 0;

  Future<bool> _testConnection(String endpoint, int totalEndpoints) async {
    WebSocket? ws;
    testedEndpoints++;

    try {
      ws = await WebSocket.connect(endpoint);
      var rpcRequest = jsonEncode(
          {"jsonrpc": "2.0", "method": "system_health", "params": [], "id": 1});
      ws.add(rpcRequest);
      await for (var message in ws) {
        var response = jsonDecode(message);
        if (response['id'] == 1) {
          if (response.containsKey('error')) {
            throw RpcException(response['error']);
          }
          break;
        }
      }

      if (testedEndpoints == totalEndpoints && errorLogs.isNotEmpty) {
        log.d(
            'Duniter endpoints errors (${errorLogs.length}/$totalEndpoints):\n${errorLogs.join('\n')}');
      }

      return true;
    } catch (e) {
      errorLogs.add(e.toString());
      return false;
    } finally {
      ws?.close();
    }
  }

  Future<String> getFastestEndpoint(List<String> endpoints) async {
    final endpointStream = Stream.fromIterable(endpoints);
    final completer = Completer<String>();
    const timeoutSeconds = 5;
    final totalEndpoints = endpoints.length;

    endpointStream
        .asyncMap((endpoint) async {
          return await _testConnection(endpoint, totalEndpoints)
              ? endpoint
              : throw ConnectionException(endpoint);
        })
        .timeout(
          Duration(seconds: timeoutSeconds),
          onTimeout: (sink) =>
              sink.addError(TimeoutException('Connection timeout')),
        )
        .listen(
          (endpoint) {
            if (!completer.isCompleted) {
              completer.complete(endpoint);
            }
          },
          onError: (error) {
            if (!completer.isCompleted) {
              completer.completeError(error);
            }
          },
        );

    return completer.future;
  }
}

class RpcException implements Exception {
  final String message;

  RpcException(this.message);

  @override
  String toString() => 'Error in RPC response: $message';
}

class ConnectionException implements Exception {
  final String endpoint;

  ConnectionException(this.endpoint);

  @override
  String toString() => 'Failed to connect to $endpoint';
}
