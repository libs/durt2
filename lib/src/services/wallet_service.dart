import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:math';
import 'dart:typed_data';
import 'package:durt2/src/global.dart';
import 'package:durt2/src/models/encrypted_mnemonic.dart';
import 'package:durt2/src/models/retrieve_mnemonic_args.dart';
import 'package:durt2/src/models/safe_box.dart';
import 'package:durt2/src/models/wallet_data.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:pointycastle/export.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class WalletService {
  static Future<Box<EncryptedMnemonic>> openEncryptedBox() async {
    final encryptionKey = await getEncryptionKey();
    final hiveAesCipher = HiveAesCipher(encryptionKey);

    return await Hive.openBox<EncryptedMnemonic>('mnemonics',
        encryptionCipher: hiveAesCipher);
  }

  static Future<void> clearWallet() async {
    const _secureStorage = FlutterSecureStorage();
    await Hive.deleteBoxFromDisk('mnemonics');
    await walletDataBox.clear();
    await _secureStorage.delete(key: 'hive_encryption_key');
  }

  static Future<Uint8List> getEncryptionKey() async {
    const _secureStorage = FlutterSecureStorage();
    final base64Key = await _secureStorage.read(key: 'hive_encryption_key');
    if (base64Key != null) {
      return base64Decode(base64Key);
    } else {
      final key = Hive.generateSecureKey();
      await _secureStorage.write(
          key: 'hive_encryption_key', value: base64Encode(key));
      return Uint8List.fromList(key);
    }
  }

  static int getDefaultSafeBoxNumber() {
    return configBox.get('defaultSafeBoxNumber', defaultValue: 0);
  }

  static SafeBox? getSafeBox(int safeBoxNumber) {
    return safeBox.get(safeBoxNumber);
  }

  static Future setDefaultSafeBoxNumber(int safeBoxNumber) async {
    await configBox.put('defaultSafeBoxNumber', safeBoxNumber);
  }

  static WalletData? getWalletData(String address, [int? safeBoxNumber]) {
    safeBoxNumber ??= getDefaultSafeBoxNumber();
    final key = WalletDataKey(safeBoxNumber: safeBoxNumber, address: address);
    return walletDataBox.get(key.hashCode);
  }

  static WalletData? getDefaultWalletData() {
    final safeBoxNumber = getDefaultSafeBoxNumber();
    final safeBox = getSafeBox(safeBoxNumber);
    if (safeBox == null) return null;
    return getWalletData(safeBox.defaultWalletAddress);
  }

  static Future storeWalletData(WalletData walletData) async {
    await walletDataBox.put(walletData.key.hashCode, walletData);
  }

  static List<WalletData> getWalletDataList([int? safeBoxNumber]) {
    safeBoxNumber ??= getDefaultSafeBoxNumber();
    return walletDataBox.values
        .where((walletData) => walletData.safeBoxNumber == safeBoxNumber!)
        .toList();
  }

  static Future<void> storeMnemonic({
    required String address,
    required String mnemonic,
    required int pinCode,
    int safeBoxNumber = 0,
  }) async {
    final safeBoxData = SafeBox(
      number: safeBoxNumber,
      defaultWalletAddress: address,
      name: 'SafeBox $safeBoxNumber',
    );
    await safeBox.put(safeBoxNumber, safeBoxData);

    final iv = _createRandomBytes(12);
    final encryptedMnemonic = EncryptedMnemonic(
      cipherText: await _encrypt(mnemonic, pinCode.toString(), iv),
      iv: iv,
    );
    final box = await openEncryptedBox();
    await box.put(safeBoxNumber, encryptedMnemonic);
    await box.close();
  }

  static bool isWalletExist() => walletDataBox.isNotEmpty;

  static Future<String> _encrypt(
    String mnemonic,
    String pin,
    Uint8List iv,
  ) async {
    final keyDerivator = await _createKeyDerivator(iv);
    final key = keyDerivator.process(utf8.encode(pin));

    final cipher = GCMBlockCipher(AESEngine());
    final params = AEADParameters(KeyParameter(key), 128, iv, Uint8List(0));

    cipher.init(true, params);

    final input = Uint8List.fromList(utf8.encode(mnemonic));
    final output = Uint8List(cipher.getOutputSize(input.length));
    var len = cipher.processBytes(input, 0, input.length, output, 0);
    len += cipher.doFinal(output, len);

    final cipherText = base64.encode(output.sublist(0, len));
    return cipherText;
  }

  static Future<String?> _decrypt(EncryptedMnemonic encryptedMnemonic,
      String pin, Uint8List encryptionKey) async {
    try {
      final keyDerivator =
          await _createKeyDerivator(encryptedMnemonic.iv, encryptionKey);
      final key = keyDerivator.process(utf8.encode(pin));

      final cipher = GCMBlockCipher(AESEngine());
      final params = AEADParameters(
          KeyParameter(key), 128, encryptedMnemonic.iv, Uint8List(0));

      cipher.init(false, params);
      final cipherData = base64.decode(encryptedMnemonic.cipherText);

      final output = Uint8List(cipher.getOutputSize(cipherData.length));
      var len =
          cipher.processBytes(cipherData, 0, cipherData.length, output, 0);
      len += cipher.doFinal(output, len);

      return String.fromCharCodes(output.sublist(0, len));
    } catch (e) {
      log.e('Failed to decrypt mnemonic: $e');
      return null;
    }
  }

  static Future<Argon2BytesGenerator> _createKeyDerivator(Uint8List salt,
      [Uint8List? encryptionKey]) async {
    final argon2 = Argon2BytesGenerator();
    encryptionKey ??= await getEncryptionKey();

    argon2.init(
      Argon2Parameters(
        Argon2Parameters.ARGON2_id,
        salt,
        secret: encryptionKey,
        desiredKeyLength: 32,
        iterations: 3,
        memoryPowerOf2: 16,
        lanes: 4,
        version: Argon2Parameters.ARGON2_VERSION_13,
      ),
    );
    return argon2;
  }

  static Uint8List _createRandomBytes(int length) {
    final secureRandom = FortunaRandom();
    final seedSource = Random.secure();
    final seeds = <int>[];
    for (int i = 0; i < 32; i++) {
      seeds.add(seedSource.nextInt(255));
    }
    secureRandom.seed(KeyParameter(Uint8List.fromList(seeds)));

    final bytes = secureRandom.nextBytes(length);
    return bytes;
  }

  static Future<String?> retrieveMnemonic({
    required int safeBoxNumber,
    required String pinCode,
    required Uint8List encryptionKey,
  }) async {
    final hiveAesCipher = HiveAesCipher(encryptionKey);
    final encryptedBox = await Hive.openBox<EncryptedMnemonic>('mnemonics',
        encryptionCipher: hiveAesCipher);

    final encryptedMnemonic = encryptedBox.get(safeBoxNumber);
    await encryptedBox.close();
    if (encryptedMnemonic == null) {
      return null;
    }

    return await _decrypt(encryptedMnemonic, pinCode, encryptionKey);
  }

  static Future<String?> retriveMnemonicIsolate(
      int safeBoxNumber, String pin) async {
    final pinCode = int.parse(pin);

    final encryptionKey = await getEncryptionKey();
    final appDocDir = await getApplicationDocumentsDirectory();
    final appDir = Directory(path.join(appDocDir.path, 'durt2'));
    if (!await appDir.exists()) {
      await appDir.create(recursive: true);
    }

    final receivePort = ReceivePort();
    final retrieveMnemonicArgs = RetrieveMnemonicArgs(
      safeBoxNumber: safeBoxNumber,
      encryptionKey: encryptionKey,
      appDir: appDir.path,
      sendPort: receivePort.sendPort,
    );

    await Isolate.spawn(retrieveMnemonicIsolate, retrieveMnemonicArgs);

    final message = await receivePort.first as List;
    final isolateSendPort = message[0] as SendPort;
    final replyTo = ReceivePort();
    isolateSendPort.send([pinCode, replyTo.sendPort]);

    return await replyTo.first as String?;
  }
}

void retrieveMnemonicIsolate(RetrieveMnemonicArgs args) {
  Hive.init(args.appDir);
  Hive.registerAdapter(EncryptedMnemonicAdapter());

  final receivePort = ReceivePort();
  final streamController = StreamController<bool>();

  receivePort.listen((message) async {
    final pinCode = message[0] as int;
    final replyTo = message[1] as SendPort;
    final mnemonic = await WalletService.retrieveMnemonic(
      safeBoxNumber: args.safeBoxNumber,
      pinCode: pinCode.toString(),
      encryptionKey: args.encryptionKey,
    );

    replyTo.send(mnemonic);
    // Close the receive port and stream controller when done
    receivePort.close();
    streamController.close();
  });

  args.sendPort.send([receivePort.sendPort, streamController.sink.add]);
}
